#include "ros/ros.h"
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>
#include <iostream>
#include <math.h>
#include <octomap/MapNode.h>
#include <octomap/OcTree.h>
#include <octomap/MapCollection.h>
#include <inspection_by_planes/CheckCollision.h>
using namespace std;
using namespace octomap;

class CollisionCheck
{
public:
	CollisionCheck()
	{
	std::string servname = "octomap_binary";
	octomap_msgs::GetOctomap::Request req;
	octomap_msgs::GetOctomap::Response resp;
	while (true)
	{
		ROS_WARN("Request to creat octree failed; trying again...");
		usleep(1000000);
		OcTree* octree = NULL;
		AbstractOcTree* tree = octomap_msgs::msgToMap(resp.map);
        if (ros::service::call(servname, req, resp)){
	       	if (tree){
	        	octree = dynamic_cast<OcTree*>(tree);
	       	}

	       	else {	        
	        	if (resp.map.id == "ColorOcTree")
	           		ROS_WARN("You requested a binary map for a ColorOcTree - this is currently not supported. Please add -f to request a full map");
	           }

	        if (octree and octree->size() > 0){
	        	ROS_INFO("Map received (%zu nodes, %f m res)", octree->size(), octree->getResolution());
	        	MapNode<OcTree>* mn1 = new MapNode<OcTree>(octree, pose6d(0.0,0.0,0.0,0.0,0.0,0.0));
	        	mn1->setId("Node");
	        	mapcollection.addNode(mn1);
	        	break;
	     	}	
		}
	}	
	}

	bool check_collision(inspection_by_planes::CheckCollision::Request  &req, inspection_by_planes::CheckCollision::Response &res)
	{
		res.occupied = 0;
		if (mapcollection.isOccupied(req.x,req.y,req.z))
        {
			res.occupied = 1;
			return true;
		}
		double current[] = {req.x, req.y, req.z};
		for (int k = 0; k <12; k ++)
			{
                double neighbor[3];
                for (int i = 0; i < 3; i++)
                {
                neighbor[i] = current[i] + dir_vect[k][i] * 1;
                }
                if (mapcollection.isOccupied(neighbor[0], neighbor[1], neighbor[2]))
                {
					res.occupied = 1;
					return true;
				}
			}
		return true;
	}

private:
	MapCollection<MapNode<OcTree> > mapcollection;
	double dir_vect[12][3] = {{1,1,0}, {1,-1,0}, {-1,1,0}, {-1,-1,0},
						  {1,1,0.5}, {1,-1,0.5}, {-1,1,0.5}, {-1,-1,0.5},
						  {1,1,-0.5}, {1,-1,-0.5}, {-1,1,-0.5}, {-1,-1,-0.5}};
};

int main(int argc, char **argv)
{
	ros::init(argc, argv, "");
	ros::NodeHandle nh;
	CollisionCheck cl;
	ros::ServiceServer service = nh.advertiseService("checking_collision", &CollisionCheck::check_collision, &cl);
	ros::spin();
	return 0;
}