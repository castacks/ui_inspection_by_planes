from numpy import matrix
import numpy as np
import yaml
import math
import rospy
from geometry_msgs.msg import Pose, PoseArray, Point
from visualization_msgs.msg import Marker
from std_msgs.msg import ColorRGBA
import rospkg
from copy import deepcopy
from inspection_by_planes.srv import *


class PathPlanner(object):

    def __init__(self, rm, ps, hover, dist):
        pub_plan = rospy.Publisher(
            'inspection_plan', PoseArray, queue_size=100, latch=True)
        path = PoseArray()
        path.header.stamp = rospy.Time.now()
        path.header.frame_id = "world"
        if len(rm) == 0:
            pub_plan.publish(path)
            self.display_path(path, 0)
            return

        # hover = np.asarray(hover)
        # rospy.wait_for_service("checking_collision")
        # check_collision = rospy.ServiceProxy(
        #     'checking_collision', CheckCollision)
        # resp1 = check_collision(hover[0], hover[1], hover[2])
        # check_collision.close()
        # if abs(resp1.occupied - 1) < 0.001:
        #     rospy.logwarn("Error2 : Hover Point is not a valid robot state!!!")
        #     return

        rospack = rospkg.RosPack()
        cam_config = rospack.get_path(
            'inspection_by_planes') + "/config/cam_config.yaml"
        cam_specs = self.get_cam_config(cam_config)
        last_point = deepcopy(hover)
        time = rospy.Time.now()

        for i in range(len(rm)):
            poses = self.get_waypoints(
                cam_specs, rm[i], ps[i][0], ps[i][1], ps[i][2], ps[i][3], hover, dist).poses
            if len(poses) == 0: #@TODO : Change condition for multiple planes
                rospy.logwarn("0 poses in path. Check area for inspection")
                return

            first_point = [
                poses[0].position.x, poses[0].position.y, poses[0].position.z]
            path.poses.extend(self.waypoints_a_to_b(
                hover, np.asarray(first_point)).poses)

            path.poses.extend(poses)
            last_point = [path.poses[-1].position.x, path.poses[-1].position.y, path.poses[-1].position.z]        

        path.poses.extend(
            self.waypoints_a_to_b(np.asarray(last_point), hover).poses)

        viable_path = self.display_path(path)
        if not viable_path:
            del path.poses[:]
        pub_plan.publish(path)
        return

    def get_rel_pos(self, rot_mat, pos):
        pos = np.dot(rot_mat.I, matrix([[pos[0]], [pos[1]], [pos[2]]]))
        return [pos.item(0), pos.item(1), pos.item(2)]

    def get_cam_config(self, str):
        stream = open(str, "r")
        cam_specs = {}
        docs = yaml.load(stream)
        for k, v in docs.items():
            cam_specs[k] = v
        cam_specs["hor_max_fov"] = cam_specs["smallest_feature_size"] * \
            cam_specs["hor_resolution"] / cam_specs["pixels_per_feature"]
        cam_specs["ver_max_fov"] = cam_specs["smallest_feature_size"] * \
            cam_specs["ver_resolution"] / cam_specs["pixels_per_feature"]
        cam_specs[
            "max_d"] = min(cam_specs["hor_max_fov"] * cam_specs["focal_length"] / cam_specs["hor_sensor_size"],
                           cam_specs["ver_max_fov"] * cam_specs["focal_length"] / cam_specs["ver_sensor_size"])
        return cam_specs

    def waypoints_a_to_b(self, a, b, gap=3.0):
        distance = np.linalg.norm(b - a)
        if distance == 0:
            distance = 1;
        dir_vect = (b - a) / distance
        num_wp = int(math.ceil(distance / gap))
        gap = distance / num_wp
        path = PoseArray()
        for i in range(0, num_wp + 1):
            path.poses.append(self.get_pose(a + i * gap * dir_vect, [1, 0, 0]))
        return path

    def get_pose(self, position, vector):
        pose = Pose()
        pose.position.x = position[0]
        pose.position.y = position[1]
        pose.position.z = position[2]
        # Calculating the half-way vector.
        u = np.asarray([1, 0, 0])
        norm = np.linalg.norm(vector)
        v = np.asarray(vector) / norm
        if (np.array_equal(u, v)):
            pose.orientation.w = 1
            pose.orientation.x = 0
            pose.orientation.y = 0
            pose.orientation.z = 0
        elif (np.array_equal(u, -v)):
            pose.orientation.w = 0
            pose.orientation.x = 0
            pose.orientation.y = 0
            pose.orientation.z = 1
        else:
            half = [u[0] + v[0], u[1] + v[1], u[2] + v[2]]
            pose.orientation.w = np.dot(u, half)
            temp = np.cross(u, half)
            pose.orientation.x = temp[0]
            pose.orientation.y = temp[1]
            pose.orientation.z = temp[2]
            norm = math.sqrt(
                pose.orientation.x * pose.orientation.x +
                pose.orientation.y * pose.orientation.y +
                pose.orientation.z * pose.orientation.z +
                pose.orientation.w * pose.orientation.w)
            if norm == 0:
                norm = 1
            pose.orientation.x /= norm
            pose.orientation.y /= norm
            pose.orientation.z /= norm
            pose.orientation.w /= norm
        return pose

    def get_waypoints(self, cam_specs, rot_mat, a, b, c, d, hover, dist):
        dist_from_wall = min(abs(dist), cam_specs["max_d"])
        ymin = min(a[1], b[1], c[1], d[1])
        zmin = min(a[2], d[2], c[2], b[2])
        ymax = max(a[1], b[1], c[1], d[1])
        zmax = max(a[2], d[2], c[2], b[2])

        hor_max_fov = float(
            "{0:.2f}".format(dist_from_wall * cam_specs["hor_sensor_size"] / cam_specs["focal_length"]))
        ver_max_fov = float(
            "{0:.2f}".format(dist_from_wall * cam_specs["ver_sensor_size"] / cam_specs["focal_length"]))
        wall_l = ymax - ymin
        wall_b = zmax - zmin
        ver_div = int(math.ceil(wall_l / ver_max_fov))
        ver_max_fov = wall_l * 1.0 / ver_div
        hor_div = int(math.ceil(wall_b / hor_max_fov))
        hor_max_fov = wall_b * 1.0 / hor_div
        x = a[0] - dist_from_wall
        vector = self.get_rel_pos(rot_mat, [1, 0, 0])
        path = PoseArray()
        fromymin = 0  # Start inspection from ymin by default
        if abs(hover[1] - ymin) > abs(hover[1] - ymax):
            fromymin = 1
        for row in range(hor_div):
            z = zmin + (row + 0.5) * hor_max_fov
            for col in range(ver_div):
                if (row + fromymin) % 2 != 0:
                    mod_col = (ver_div - 1) - col
                else:
                    mod_col = col
                y = ymin + (mod_col + 0.5) * ver_max_fov
                pose = self.get_pose(
                    self.get_rel_pos(rot_mat, [x, y, z]), vector)
                path.poses.append(pose)

        return path

    def display_path(self, path_orig, delta=0.5):
        pub_path = rospy.Publisher('path', Marker, queue_size=10, latch=True)
        viable_path = True
        marker = Marker()
        marker.header.frame_id = "world"
        marker.header.stamp = rospy.Time()
        marker.ns = "path"
        marker.id = 0
        marker.type = Marker.LINE_STRIP
        marker.action = Marker.ADD
        marker.pose.orientation.w = 1.0
        marker.scale.x = 0.1
        marker.scale.y = 0.1
        marker.scale.z = 0.1
        marker.color.a = 1
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 1.0
        marker.points = []
        marker.colors = []
        path = PoseArray()
        for i in range(len(path_orig.poses)-1):
            path.poses.extend(
                self.waypoints_a_to_b(
                    np.asarray([path_orig.poses[i].position.x, path_orig.poses[i].position.y, path_orig.poses[i].position.z]),
                    np.asarray([path_orig.poses[i+1].position.x, path_orig.poses[i+1].position.y, path_orig.poses[i+1].position.z]), delta).poses)
        rospy.wait_for_service("checking_collision")
        check_collision = rospy.ServiceProxy('checking_collision', CheckCollision, persistent=True)
        for i in range(len(path.poses)):
            point = Point()
            point.x = path.poses[i].position.x
            point.y = path.poses[i].position.y
            point.z = path.poses[i].position.z             
            try:
                resp1 = check_collision(point.x, point.y, point.z)
                collision = resp1.occupied
                if collision ==1:
                    viable_path = False
            except rospy.ServiceException, e:
                print "Service Call Failed: %s Click Again"%e,
                continue
            c = ColorRGBA()
            c.a = 1
            c.g = 29.0/255
            c.b = 240.0/255
            if collision == 1:
                c.g = 0
                c.b = 0
            marker.points.append(point)
            marker.colors.append(c)
        if not viable_path:
            for i in range(len(marker.colors)):
                marker.colors[i].r = 1

        check_collision.close()
        pub_path.publish(marker)
        return viable_path
