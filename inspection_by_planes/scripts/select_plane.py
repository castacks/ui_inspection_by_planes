#!/usr/bin/env python

import rospy
from copy import deepcopy
from interactive_markers.interactive_marker_server import *
from interactive_markers.menu_handler import *
import tf
from visualization_msgs.msg import *
import math
import numpy as np
from numpy import matrix
# from inspection_by_planes.msg import Inspection3d
from inspection_by_planes.path_planner import PathPlanner
from inspection_by_planes.srv import *
from geometry_msgs.msg import Point
from Queue import LifoQueue

positions = list()
quat = list()
rot_mat = list()
positions_mid = list()
menu_handler = MenuHandler()
server = None
selected_plane = None
feedbackqueue = LifoQueue()


def get_quat(plane_num):
    # Compute Quaternion
    a = rot_mat[plane_num].I
    trace = a[0, 0] + a[1, 1] + a[2, 2]
    if(trace > 0):
        s = 0.5 / math.sqrt(trace + 1.0)
        qw = 0.25 / s
        qx = (a[2, 1] - a[1, 2]) * s
        qy = (a[0, 2] - a[2, 0]) * s
        qz = (a[1, 0] - a[0, 1]) * s
    else:
        if (a[0, 0] > a[1, 1] and a[0, 0] > a[2, 2]):
            s = 2.0 * math.sqrt(1.0 + a[0, 0] - a[1, 1] - a[2, 2])
            qw = (a[2, 1] - a[1, 2]) / s
            qx = 0.25 * s
            qy = (a[0, 1] + a[1, 0]) / s
            qz = (a[0, 2] + a[2, 0]) / s
        elif (a[1, 1] > a[2, 2]):
            s = 2.0 * math.sqrt(1.0 + a[1, 1] - a[0, 0] - a[2, 2])
            qw = (a[0, 2] - a[2, 0]) / s
            qx = (a[0, 1] + a[1, 0]) / s
            qy = 0.25 * s
            qz = (a[1, 2] + a[2, 1]) / s
        else:
            s = 2.0 * math.sqrt(1.0 + a[2, 2] - a[0, 0] - a[1, 1])
            qw = (a[1, 0] - a[0, 1]) / s
            qx = (a[0, 2] + a[2, 0]) / s
            qy = (a[1, 2] + a[2, 1]) / s
            qz = 0.25 * s
    return [qx, qy, qz, qw]


def get_rm(plane):
    # Compute rotation matrix
    mid_point = (np.asarray(plane[4:7]) + np.asarray(
        plane[7:10]) + np.asarray(plane[10:13]) + np.asarray(plane[13:16])) * 0.25
    i1 = np.asarray(plane[0:3])
    i1 = i1 / np.linalg.norm(i1)
    if np.dot(np.zeros(3) - mid_point, i1) > np.dot(np.zeros(3) - mid_point, -i1):
        i1 = i1 * -1.0

    if (abs(plane[5] - plane[8]) > abs(plane[6] - plane[9]) or
            abs(plane[4] - plane[7]) > abs(plane[6] - plane[9])):
        k1 = np.asarray(plane[4:7]) - np.asarray(plane[13:16])
    else:
        k1 = np.asarray(plane[4:7]) - np.asarray(plane[7:10])
    k1 = k1 / np.linalg.norm(k1)
    j1 = -np.cross(i1, k1)
    j1 = j1 / np.linalg.norm(j1)

    i = np.asarray([1, 0, 0])
    j = np.asarray([0, 1, 0])
    k = np.asarray([0, 0, 1])
    rm = matrix([[sum(i * i1), sum(j * i1), sum(k * i1)],
                [sum(i * j1), sum(j * j1), sum(k * j1)],
                [sum(i * k1), sum(j * k1), sum(k * k1)]])
    return rm


def get_rel_pos(pos, plane_num):
    pos = np.dot(rot_mat[plane_num], matrix([[pos[0]], [pos[1]], [pos[2]]]))
    return [pos.item(0), pos.item(1), pos.item(2)]


def processFeedback(feedback):
    global feedbackqueue
    if feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
        feedbackqueue.put(feedback)
    return


def feedback_callback(msg):
    global feedbackqueue
    if feedbackqueue.empty():
        return
    feedback = feedbackqueue.get()
    feedbackqueue = LifoQueue()

    x = feedback.pose.position.x
    y = feedback.pose.position.y
    z = feedback.pose.position.z
    index = int(feedback.marker_name)

    # Hover point Feedback
    if index == len(positions) - 1:
        positions[index][0] = x
        positions[index][1] = y
        positions[index][2] = z
        pose = geometry_msgs.msg.Pose()
        print "Moving Hover point to", positions[index], "\n"
        pose.position.x = positions[index][0]
        pose.position.y = positions[index][1]
        pose.position.z = positions[index][2]
        server.setPose(str(index), pose)
        
        #Check validity of hover point
        rospy.wait_for_service("checking_collision")
        check_collision = rospy.ServiceProxy(
            'checking_collision', CheckCollision)
        resp1 = check_collision(x, y, z)
        check_collision.close()
        int_marker = server.get(str(index))
        for control in int_marker.controls:
            for marker in control.markers:
                if abs(resp1.occupied - 1) < 0.01:
                    rospy.logwarn("Error : Hover Point is not a valid robot state!!!")
                    PathPlanner([], [], positions[-1], 1)
                    marker.color.r = 1.0
                    marker.color.g = 0.0  
                else:
                    marker.color.g = 1.0
                    marker.color.r = 0.0
                    if selected_plane is not None:
                        inspect_plane(selected_plane)
        server.insert(int_marker, processFeedback)
        server.applyChanges()
        return

    # Planes Feedback
    new_pos = [x, y, z]
    plane_num = index / 4
    pt_num = index - plane_num * 4

    # Checking aligning of points
    check_ab = 1
    check_ad = 2
    if abs(positions[plane_num * 4][2] - positions[plane_num * 4 + 1][2]) < math.exp(-3):
        check_ab = 2
        check_ad = 1
    # move plan markers to retain rectangle
    if pt_num == 0:
        positions[index][1] = y
        positions[index][2] = z
        positions[index + 3][check_ad] = new_pos[check_ad]
        positions[index + 1][check_ab] = new_pos[check_ab]
    elif pt_num == 1:
        positions[index][1] = y
        positions[index][2] = z
        positions[index + 1][check_ad] = new_pos[check_ad]
        positions[index - 1][check_ab] = new_pos[check_ab]
    elif pt_num == 2:
        positions[index][1] = y
        positions[index][2] = z
        positions[index + 1][check_ab] = new_pos[check_ab]
        positions[index - 1][check_ad] = new_pos[check_ad]
    elif pt_num == 3:
        positions[index][1] = y
        positions[index][2] = z
        positions[index - 3][check_ad] = new_pos[check_ad]
        positions[index - 1][check_ab] = new_pos[check_ab]
    else:
        print "error: check planes!\n"

    mid_point = np.asarray([0.0, 0.0, 0.0])
    print "Adjusting Plane", plane_num
    for i in range(plane_num * 4, plane_num * 4 + 4):
        pose = geometry_msgs.msg.Pose()
        pose.position.x = positions[i][0]
        pose.position.y = positions[i][1]
        pose.position.z = positions[i][2]
        mid_point = mid_point + np.asarray(positions[i])
        server.setPose(str(i), pose)

    # Adjust Plane dimensions
    zlen = (max(positions[plane_num * 4][2], positions[plane_num * 4 + 1][2], positions[plane_num * 4 + 2][2], positions[plane_num * 4 + 3][2]) -
            min(positions[plane_num * 4][2], positions[plane_num * 4 + 1][2], positions[plane_num * 4 + 2][2], positions[plane_num * 4 + 3][2]))
    ylen = (max(positions[plane_num * 4][1], positions[plane_num * 4 + 1][1], positions[plane_num * 4 + 2][1], positions[plane_num * 4 + 3][1]) -
            min(positions[plane_num * 4][1], positions[plane_num * 4 + 1][1], positions[plane_num * 4 + 2][1], positions[plane_num * 4 + 3][1]))
    mid_point = mid_point * 0.25
    int_marker = server.get("plane " + str(plane_num))
    pose = geometry_msgs.msg.Pose()
    int_marker.pose.position.x = mid_point[0]
    int_marker.pose.position.y = mid_point[1]
    int_marker.pose.position.z = mid_point[2]
    for control in int_marker.controls:
        for marker in control.markers:
            marker.scale.y = ylen
            marker.scale.z = zlen
    server.insert(int_marker, menuFeedback)
    server.applyChanges()
    inspect_plane(plane_num)
    return


def menuFeedback(feedback):
    global selected_plane
    plane_num = int(feedback.marker_name.split(" ")[-1])

    if feedback.event_type == InteractiveMarkerFeedback.MENU_SELECT:
        # if feedback.menu_entry_id == 2:
        print "Returning to view all planes\n"
        for i in range(plane_num * 4, plane_num * 4 + 4):
            server.erase(str(i))
        PathPlanner([], [], positions[-1], 1)
        set_plane_visibility(plane_num, False)
        selected_plane = None

    # if feedback.menu_entry_id == 1:
    if feedback.event_type == InteractiveMarkerFeedback.BUTTON_CLICK:
        if plane_num == selected_plane:
            inspect_plane(plane_num)
            return
        print "Diplaying Plane", plane_num, "\n"
        for i in range(plane_num * 4, plane_num * 4 + 4):
            int_marker = InteractiveMarker()
            int_marker.pose.position.x = positions[i][0]
            int_marker.pose.position.y = positions[i][1]
            int_marker.pose.position.z = positions[i][2]
            int_marker.header.frame_id = str(i / 4)
            int_marker.name = str(i)
            makeBoxControl(int_marker)
            server.insert(int_marker, processFeedback)
        set_plane_visibility(plane_num, True)
        selected_plane = plane_num
        inspect_plane(plane_num)
    return


def inspect_plane(plane):
    rm = [rot_mat[plane]]
    dist_from_wall = rospy.get_param("dist_from_wall")
    ps = [[positions[plane * 4], positions[plane * 4 + 1],
           positions[plane * 4 + 2], positions[plane * 4 + 3]]]
    PathPlanner(rm, ps, positions[-1], dist_from_wall)
    return


def set_plane_visibility(plane_num, sel):
    if sel:
        plane_vis = 0.0
        scale = [0.001, 0.001, 0.001]
    else:
        plane_vis = 0.4
    number_planes = len(positions) / 4
    for i in range(number_planes):
        if i == plane_num:
            continue
        int_marker = server.get("plane " + str(i))
        for control in int_marker.controls:
            for marker in control.markers:
                marker.color.a = plane_vis
                if sel is not True:
                    zlen = (max(positions[i * 4][2], positions[i * 4 + 1][2], positions[i * 4 + 2][2], positions[i * 4 + 3][2]) -
                            min(positions[i * 4][2], positions[i * 4 + 1][2], positions[i * 4 + 2][2], positions[i * 4 + 3][2]))
                    ylen = (max(positions[i * 4][1], positions[i * 4 + 1][1], positions[i * 4 + 2][1], positions[i * 4 + 3][1]) -
                            min(positions[i * 4][1], positions[i * 4 + 1][1], positions[i * 4 + 2][1], positions[i * 4 + 3][1]))
                    scale = [0.1, ylen, zlen]
                marker.scale.x = scale[0]
                marker.scale.y = scale[1]
                marker.scale.z = scale[2]
        server.insert(int_marker, menuFeedback)
    server.applyChanges()


def makeBox(msg, args):
    clr = args[0]
    scale = args[1]
    marker = Marker()
    marker.type = Marker.CUBE
    marker.scale.x = scale[0]
    marker.scale.y = scale[1]
    marker.scale.z = scale[2]
    marker.color.r = clr[0]
    marker.color.g = clr[1]
    marker.color.b = clr[2]
    marker.color.a = clr[3]
    return marker


def makeBoxControl(msg, args=[[1.0, 0.0, 0.0, 1.0], [0.1, 0.3, 0.3], [InteractiveMarkerControl.MOVE_PLANE]]):
    control = InteractiveMarkerControl()
    for ctrl in args[2]:
        control.interaction_mode = ctrl
        msg.controls.append(deepcopy(control))
    control.always_visible = True
    control.markers.append(makeBox(msg, args[:2]))
    msg.controls.append(control)
    return


def planeInteraction(p):
    plane_num = 0
    for i in p:
        a = get_rel_pos(i[4:7], plane_num)
        b = get_rel_pos(i[7:10], plane_num)
        c = get_rel_pos(i[10:13], plane_num)
        d = get_rel_pos(i[13:16], plane_num)
        positions.extend([a, b, c, d])
        int_marker = InteractiveMarker()
        mid_point = (np.asarray(a) + np.asarray(b) +
                     np.asarray(c) + np.asarray(d)) * 0.25
        int_marker.pose.position.x = mid_point[0]
        int_marker.pose.position.y = mid_point[1]
        int_marker.pose.position.z = mid_point[2]
        positions_mid.append(mid_point)
        int_marker.header.frame_id = str(plane_num)
        int_marker.name = "plane " + str(plane_num)
        ylen = max(a[1], b[1], c[1], d[1]) - min(a[1], b[1], c[1], d[1])
        zlen = max(a[2], d[2], c[2], b[2]) - min(a[2], d[2], c[2], b[2])
        col = 1.0 - (1.0 * plane_num) / len(p)
        makeBoxControl(
            int_marker, [[0.0, 0.0, col, 0.4],
                         [0.1, ylen, zlen], [InteractiveMarkerControl.MENU, InteractiveMarkerControl.BUTTON]])
        server.insert(int_marker, menuFeedback)
        menu_handler.apply(server, int_marker.name)
        plane_num = plane_num + 1

    i = len(p) * 4
    # Initialize hover point
    int_marker = InteractiveMarker()
    int_marker.pose.position.x = 0
    int_marker.pose.position.y = 0
    int_marker.pose.position.z = -1.5
    positions.append([0, 0, -1.5])
    int_marker.header.frame_id = "world"
    int_marker.name = str(i)
    makeBoxControl(
        int_marker, [[0.0, 1.0, 0.0, 1.0], [0.5, 0.5, 0.5], [InteractiveMarkerControl.MOVE_ROTATE_3D]])
    server.insert(int_marker, processFeedback)
    return


def frameCallback(msg):
    # Publish TF frames
    for i in range(len(quat)):
        br = tf.TransformBroadcaster()
        br.seq = i
        br.sendTransform(
            (0, 0, 0), (quat[i][0], quat[i][1], quat[i][2], quat[i][3]), rospy.Time.now(), str(i), "world")
    return


# def start_plan(request):
#     # Subscribed to rqt plugin
#     planes = [i for i in request.planes]
#     dist_from_wall = request.dist_from_wall
#     rm = []
#     ps = []
#     for plane in planes:
#         rm.append(rot_mat[plane])
#         ps.append(
#             [positions[plane * 4], positions[plane * 4 + 1], positions[plane * 4 + 2], positions[plane * 4 + 3]])
#     PathPlanner(rm, ps, positions[-1], dist_from_wall)
#     return

if __name__ == "__main__":
    rospy.init_node("inspection")
    server = InteractiveMarkerServer("all_planes")
    menu_handler.insert("Return to Selection", callback=menuFeedback)
    rospy.loginfo("initializing..")

    # Reading File for Planes
    planes_file = rospy.get_param('planes_file')
    f = open(planes_file, "r")
    planes = []
    for line in f:
        if len(line.split("  ")) < 2:
            continue
        planes.append([float(i) for i in line.split("  ") if i[-2:] != '\n'])
    f.close()

    # Compute Plane Transformations
    for i in range(len(planes)):
        rot_mat.append(get_rm(planes[i]))
        quat.append(get_quat(i))

    rospy.loginfo("Publishing TF Frames..")
    rospy.Timer(rospy.Duration(2), frameCallback)
    planeInteraction(planes)
    rospy.Timer(rospy.Duration(1), feedback_callback)
    # rospy.Subscriber('ui_inspection_request', Inspection3d, start_plan)
    server.applyChanges()
    rospy.spin()
