cmake_minimum_required(VERSION 2.8.3)
project(inspection_ui)

##################
## Dependencies ##
##################
find_package(catkin REQUIRED COMPONENTS
  rospy
  rqt_gui
  rqt_gui_py
)

############
## Python ##
############

catkin_python_setup()

############
## catkin ##
############

catkin_package()


#############
## Install ##
#############

install(PROGRAMS
  src/inspection_ui/inspection_3d_plugin.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)