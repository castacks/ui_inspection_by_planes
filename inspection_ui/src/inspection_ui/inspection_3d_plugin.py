from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QWidget
from python_qt_binding.QtCore import SIGNAL
import rospkg
import rospy
from std_msgs.msg import *
from inspection_by_planes.msg import Inspection3d
import os
import copy


class Inspection3dPlugin(Plugin):
    
    def __init__(self, context):
        super(Inspection3dPlugin, self).__init__(context)
        self.setObjectName('Inspection3dPlugin')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('inspection_ui'), 'ui', 'inspection_3d.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('Inspection3dPlugin')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + ('(%d)' % context.serial_number()))
        context.add_widget(self._widget)

        self.request_publisher = rospy.Publisher('ui_inspection_request', Inspection3d, queue_size=10)

        QWidget.connect(self._widget.start_plan, SIGNAL("clicked()"), lambda:self.publish_request(start=True))
                    

    def publish_request(self, start):
        inspection3d_request = Inspection3d()
        planes_str = self._widget.planes.text()
        planes = [int(i) for i in planes_str.split(" ")]
        inspection3d_request.planes=planes
        inspection3d_request.dist_from_wall = self._widget.dist_from_wall.value()
        self.request_publisher.publish(inspection3d_request)
